<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use Exception;
use Illuminate\Http\Request;
use App\Services\UserServices;

class UserController extends Controller
{
    use MainResponseTrait;
    protected $userServices;
    public function __construct(UserServices $userServices){
        $this->userServices = $userServices;
    }
    public function index(){
        $users = $this->userServices->getAll();
        return view('admin.user.index',compact('users'));
    }
    
    public function create(UserRequest $request){
        try{
            $this->userServices->store($request);
            return $this->SuccessResponse(trans('Lưu thành công'));
        }catch(Exception $e){
            return $this->ErrorResponse(trans('Có lỗi xảy ra khi lưu'), 500);
        }
    }
}
