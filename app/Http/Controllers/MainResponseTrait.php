<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

trait MainResponseTrait
{
    /**
     * SuccessResponse
     * 
     * Creates a success response in JSON format.
     * 
     * Created by: 08/8/2023 Trinh Xuan Son
     *
     * @param string $message An optional message to be included in the response.
     * @param array $data The data to be included in the response.
     * @return JsonResponse The JSON response containing the data, message, and success status.
     */
    public function SuccessResponse($message = '', $data = []): JsonResponse
    {
        return response()->json([
            'data' => $data,
            'message' => $message,
            'success' => true,
        ]);
    }
    /**
     * ErrorResponse
     * 
     * Creates a JSON response with an error message and status code.
     * 
     * Created by: 08/8/2023 Trinh Xuan Son
     *
     * @param string $message The error message to include in the response.
     * @param int $status The HTTP status code to include in the response. Default is 404.
     * @return JsonResponse The JSON response with the error message and status code.
     */
    public function ErrorResponse($message = '', $status = Response::HTTP_NOT_FOUND): JsonResponse
    {
        return response()->json([
            'data' => [],
            'message' => $message,
            'success' => false,
        ], $status);
    }
    /**
     * DataTableResponse
     * 
     * Generates a JSON response for a DataTable.
     *
     * Created by: 15/8/2023 Trinh Xuan Son
     * 
     * @param int $draw The draw counter.
     * @param int $recordsTotal The total number of records.
     * @param int $recordsFiltered The number of records after filtering.
     * @param mixed $data The data to be included in the response.
     * @return JsonResponse The JSON response.
     */
    public function DataTableResponse($draw, $recordsTotal, $recordsFiltered, $data, $otherData = []): JsonResponse
    {
        return response()->json([
            'draw' => $draw,
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
            'data' => $data,
            'otherData' => $otherData,
        ]);
    }
    /**
     * ErrorValidateResponse
     * 
     * Tạo json trả về lỗi giống như khi validate
     * 
     * Created by: 29/9/2023 Trinh Xuan Son
     *
     * @param string $message The message to include in the response.
     * @param array $errors An array of error messages.
     * @return JsonResponse The JSON response with error messages and the given message.
     */
    public function ErrorValidateResponse($message = '', $errors = [], $data = []): JsonResponse
    {
        return response()->json([
            'errors' => $errors,
            'message' => $message,
            'data' => $data,
        ], Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
