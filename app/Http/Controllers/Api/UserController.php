<?php

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Controllers\MainResponseTrait;
use App\Http\Requests\UserRequest;
use App\Services\UserServices;
use Illuminate\Http\JsonResponse;
use Request;

class UserController extends Controller
{
    use MainResponseTrait;
    protected $userServices;
    public function __construct(UserServices $userServices){
        $this->userServices = $userServices;
    }
    public function create(Request $request){
      dd($request);
        $request->merge([
            'path_image' => '11'
          ]);
          $input = $request->only((['name', 'email', 'password', 'path_image']));
          try {
            $user = $this->userServices->store($input);
            return $this->SuccessResponse('Created User Success', ['user' => $user]);
          } catch (\Exception $e) {
            return $this->ErrorResponse($e);
          }
    }
}
