<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    // public function authorize(): bool
    // {
    //     return false;
    // }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|max:250',
            'email' => 'required|email|max:1000',
            // 'path_image' => ''
        ];
    }
    public function messages()
    {
        return [
            'name.required' => trans('Vui lòng nhập tên '),
            'email.required' => trans('Vui lòng nhập email '),
            'email.email' => trans('Vui lòng nhập đúng định dạng email  '),
            // 'phase_description.required' => trans('Vui lòng nhập mô tả giai đoạn'),
            
        ];
    }
}
