<?php

namespace App\Services;

use App\Models\User;
use App\Services\BaseService;
use Illuminate\Support\Facades\Hash;

class UserServices extends BaseService
{
    protected $user;
    public function __construct(User $user){
        $this->user = $user;
    }
    public function getAll(){
        $users = $this->user->all();
        return $users;
    }
    public function store($data)
    {
        $data['password'] = '123456';
        $data['password'] = Hash::make($data['password']);
        $user = $this->user->create($data);
        return $user;
    }
}
