const EL_LOADING = document.querySelector('.loading') || null;
/**
 * Send post request bat dong bo
 * 
 * 
 * @param {string} url - The URL to send the request to.
 * @param {object|string} data - The data to send with the request.
 * @param {function} successCallback - (Optional) A callback function to execute if the request is successful.
 * @param {function} errorCallback - (Optional) A callback function to execute if an error occurs.
 * @return {Promise} A Promise that resolves when the request is complete.
 */
async function customAxiosPost(url, data, successCallback = null, errorCallback = null) {
    try {
        const request = await axios.post(url, data);
        await request.response;

        if (successCallback != null) {
            successCallback(request);
        }
    } catch (error) {
        console.log(error);
        error = error.response;
        // if (error.status == 401) {
        //     processTokenExpire();
        // }
        if (errorCallback != null) {
            errorCallback(error);
        }
    }
}
/**
 * Displays the loading element by setting its display property to "block".
 *
 * 
 * @param {HTMLElement} EL_LOADING - The loading element to be displayed.
 */
function showLoading() {
    EL_LOADING.style.display = "block";
}
/**
 * Hides the loading element on the page.
 *
 * 
 * @param {HTMLElement} EL_LOADING - The loading element to be hidden.
 */
function hideLoading() {
    EL_LOADING.style.display = "none";
}
/**
 * Send post request bat dong bo voi loading
 *
 * 
 * @param {string} url - The URL to send the GET request to.
 * @param {object|string} data - The data to send with the request.
 * @param {function} [successCallback=null] - The callback function to execute on success.
 * @param {function} [errorCallback=null] - The callback function to execute on error.
 */
function customAxiosPostWithLoading(url, data, successCallback = null, errorCallback = null) {
    showLoading();
    const request = customAxiosPost(url, data, successCallback, errorCallback);
    request.then(() => {
        hideLoading();
    });
}
/**
 * Hiển thị thông báo
 * 
 *
 * @param {string} message - Message cần hiển thị
 * @param {string} [type='error'] - Loại thống báo success, error,... Mặc định là 'error'
 * @param {string} [title='Thông báo'] - Tiêu đề. Mặc định là 'Thông báo'
 * @param {string} [width='20em'] - Chiều dài của thông báo. Mặc định là '20em'.
 */
function showAlert(message, type = 'error', title = 'Thông báo', width = '20em') {
    Swal.fire({
        icon: type,
        title: title,
        text: message,
        width: width
    });
}
/**
 * Hiển thị thông báo
 * 
 *
 * @param {string} option
 */
function showAlertV2(option = {}, callBack = null) {
    option = {
        icon: 'error',
        title: 'Thông báo',
        width: '20em',
        text: '',
        callBackWithoutConfirm: false,
        ...option
    };
    Swal.fire({
        icon: option.icon,
        title: option.title,
        text: option.text,
        width: option.width
    }).then((result) => {
        if (result.isConfirmed && callBack != null && !option.callBackWithoutConfirm) {
            callBack();
        }
        else if (callBack != null && option.callBackWithoutConfirm) {
            callBack();
        }
    });
}