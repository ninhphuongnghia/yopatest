@section('css')
    <style>
        label.error {
            color: red;
        }
    </style>
@endsection

<div class="modal" id="modal-add" aria-hidden="true" data-flag="">
    <div class="modal-dialog modal-xl modal-dialog-centered">
        <div class="modal-content background-checkin">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">{{ __('Thêm mới người dùng') }}</h5>
                    <!-- General Form Elements -->
                    {{-- 'name','description','image','slug' --}}
                    @include('admin.layouts.includes.message')
                    <form action="" enctype="multipart/form-data" id="myForm">
                        @csrf
                        <div class="row mb-3">
                            <label for="inputText" class="col-sm-2 col-form-label">{{ __('Tên') }}</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="name" id="name"
                                    data-msg="Tên không được để trống" value="{{ old('name') }}" required>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="inputText" class="col-sm-2 col-form-label">{{ __('email') }}</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="email" id="email"
                                    data-msg="Email không được để trống" value="{{ old('email') }}" required>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button id="submit-form" data-url= "{{route('create')}}" type="button" class="btn btn-primary">Lưu</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.js"></script>
    <script>
        $(document).ready(function() {
            $("#myForm").validate({
                rules: {
                    name: "required",
                    email: "required",
                    description: "required"
                }
            });
        });

        function clearInput(parentEl) {
            const INPUTS = parentEl.find('input[type="text"],input[type="password"],input[type="date"],input[type="time"]');
            INPUTS.val('');
        }
        $(document).on('hidden.bs.modal', '#modal-add', function() {
            clearInput($(this))
            $('#email-error').hide();
            $('#name-error').hide();
        });
        $(document).on('click', '#submit-form', function() {
            customAxiosPostWithLoading(
                $('#submit-form').attr('data-url'),
                $('#myForm').serialize(),
                function(respone) {
                    showAlertV2({
                        icon: 'success',
                        text: respone.data.message,
                        callBackWithoutConfirm: true,
                    }, function () {
                        window.location.reload();
                    });
                },
                function(error) {
                    showAlert(error.data.message);
                },
            );
            
        })
    </script>
@endsection
