@extends('admin.layouts.app')
@section('css')
    <style>
        .hidden {
            display: none;
        }

        input:focus {
            outline: none;
        }

        form input {
            width: 300px;
        }

        .cart-box {
            display: flex;
            justify-content: space-between;
            margin-bottom: 20px;
        }

        .dataTable-top {
            padding: 0px 10px;
        }

        .icon {
            display: flex;
            justify-content: space-around;
        }
    </style>
@endsection
@section('content')
    <section class="section">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">{{__('Danh sách người dùng')}}</h5>
                        {{-- @include('admin.layouts.includes.message') --}}
                        <div class="cart-box">
                            <div class="dataTable-top">
                                <form>
                                    <div class="dataTable-search search" id="dataTables_filter">
                                        <label for="">{{__('Tìm kiếm')}}</label>
                                        <input class="dataTable-input" type="search" id="searchInput" name="search">
                                    </div>
                                </form>
                            </div>
                            <button id="addUser" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modal-add">+{{__('Thêm mới')}} </button>
                        </div>
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{__('Tên')}}</th>
                                    <th>{{__('email')}}</th>
                                    <th>{{__('password')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->email }}</td>
                                        <td>{{ $item->password }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @include('admin.user.create')
    </section>
@endsection
@section('script')
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.13.4/js/dataTables.bootstrap5.min.js"></script>
    <script>
        $(document).ready(function() {
            var table = $('#example').DataTable();
            $('.dataTables_length').addClass('hidden')
            $('.dataTables_filter').addClass('hidden')
            $('.dataTables_info').addClass('hidden')
            $('#searchInput').on('keyup', function() {
                table.search(this.value).draw();
            });
        });
    </script>
@endsection
